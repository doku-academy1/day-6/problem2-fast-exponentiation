public class Main {
    public static void main(String[] args) {
        System.out.println(pow(3, 4));
    }

    public static Integer pow(Integer x, Integer n) {
//        Integer result = 1;
//
//        while (n>1) {
//            if (n % 2 == 0) {
//                n /= 2;
//                result *= x * x;
//            } else {
//                n -= 1;
//                result *= x;
//            }
//        }
//        return result;

        if (n == 1) {
            return x;
        }

        if (n%2 == 0) {
            return pow(x*x, n/2);
        } else {
            return x * pow(x, n-1);
        }

    }
}